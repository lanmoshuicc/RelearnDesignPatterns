package com.demo.design.factory;

import java.util.concurrent.TimeUnit;

/**
 * @Author: chenchen19
 * @Description
 */
public interface ICacheAdapter {
    String get(String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);

}
