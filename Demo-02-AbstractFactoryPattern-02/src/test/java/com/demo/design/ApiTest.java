package com.demo.design;

import com.demo.design.cuisin.impl.CacheServiceImpl;
import com.demo.design.factory.JDKProxy;
import com.demo.design.factory.impl.EGMCacheAdapter;
import com.demo.design.factory.impl.IIRCacheAdapter;
import org.junit.Test;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {
    @Test
    public void test_CacheService() {
        CacheService proxy_EGM = JDKProxy.getProxy(CacheServiceImpl.class, new EGMCacheAdapter());
        proxy_EGM.set("user_name_01", "小傅哥");
        String val01 = proxy_EGM.get("user_name_01");
        System.out.println("测试结果：" + val01);

        CacheService proxy_IIR = JDKProxy.getProxy(CacheServiceImpl.class, new IIRCacheAdapter());
        proxy_IIR.set("user_name_01", "小傅哥");
        String val02 = proxy_IIR.get("user_name_01");
        System.out.println("测试结果：" + val02);
    }
}
