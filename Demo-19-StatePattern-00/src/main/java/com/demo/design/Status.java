package com.demo.design;

/**
 * 活动枚举类
 */
public enum Status {
    Editing,//1创建编辑
    Check,  //2待审核
    Pass,   //3审核通过（任务扫描成活动中）
    Refuse, //4审核拒绝（可以撤审到编辑状态）
    Doing,  //5活动中
    Close,  //6活动关闭
    Open    //7活动开启（任务扫描成活动中）
}
