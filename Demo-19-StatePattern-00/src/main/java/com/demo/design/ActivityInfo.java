package com.demo.design;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author: chenchen19
 * @Description 基本活动信息
 */
@Data
public class ActivityInfo {

    @ApiModelProperty("活动id")
    private String activityId;

    @ApiModelProperty("活动名称")
    private String activityName;

    @ApiModelProperty("活动状态")
    private Enum<Status> status;

    @ApiModelProperty("开始时间")
    private Date beginTime;

    @ApiModelProperty("结束时间")
    private Date endTime;
}
