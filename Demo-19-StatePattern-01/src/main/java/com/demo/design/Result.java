package com.demo.design;
import lombok.Data;
/**
 * @Author: chenchen19
 * @Description
 */

@Data
public class Result {

    public Result(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

}
