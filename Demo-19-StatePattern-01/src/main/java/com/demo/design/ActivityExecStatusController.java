package com.demo.design;

/**
 * @Author: chenchen19
 * @Description
 */

public class ActivityExecStatusController {
    public Result execStatus(String activityId,Enum<Status>beforeStatus,Enum<Status> afterStatus) {

        //1、如果是创建编辑
        if(Status.Editing.equals(beforeStatus)) {
            //待审核还是活动关闭
            if(Status.Check.equals(afterStatus)||Status.Close.equals(afterStatus)){
                ActivityService.execStatus(activityId,beforeStatus,afterStatus);
                return new Result("0000","变更状态成功");
            } else {
                return new Result("0001","状态变更拒绝");
            }
        }

        //2、如果是审核通过
        if(Status.Pass.equals(beforeStatus)) {
            // 审核拒绝、关闭、活动中
            if(Status.Refuse.equals(afterStatus)||Status.Close.equals(afterStatus)||Status.Doing.equals(afterStatus)){
                ActivityService.execStatus(activityId,beforeStatus,afterStatus);
                return new Result("0000","变更状态成功");
            } else {
                return new Result("0001","状态变更拒绝");
            }
        }

        //3、如果是审计拒绝
        if(Status.Refuse.equals(beforeStatus)) {
            //创建编辑、关闭
            if(Status.Editing.equals(afterStatus)||Status.Close.equals(afterStatus)) {
                return new Result("0000","变更状态成功");
            } else {
                return new Result("0001","状态变更拒绝");
            }
        }

        //4、如果是活动中
        if(Status.Doing.equals(beforeStatus)) {
            //关闭
            if(Status.Close.equals(afterStatus)) {
                return new Result("0000","变更状态成功");
            } else {
                return new Result("0001","状态变更拒绝");
            }
        }

        //5、活动关闭
        if(Status.Close.equals(beforeStatus)) {
            // 活动开启
            if(Status.Open.equals(afterStatus)) {
                return new Result("0000","变更状态成功");
            } else {
                return new Result("0001","状态变更拒绝");
            }
        }

        //6、活动开启
        if(Status.Open.equals(beforeStatus)) {
            //活动关闭
            if(Status.Close.equals(afterStatus)) {
                return new Result("0000","变更状态成功");
            } else {
                return new Result("0001","状态变更拒绝");
            }
        }

        return new Result("0001", "非可处理的活动状态变更");
    }
}
