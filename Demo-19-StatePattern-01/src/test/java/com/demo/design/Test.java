package com.demo.design;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author: chenchen19
 * @Description
 */

@Slf4j
public class Test {
    @org.junit.Test
    public void test() {
        String activityId = "1001";
        ActivityService.init(activityId,Status.Editing);
        ActivityExecStatusController activityExecStatusController = new ActivityExecStatusController();
        Result refuseResult = activityExecStatusController.execStatus(activityId,Status.Editing,Status.Refuse);
        log.info("测试结果（编辑中->审核拒绝）：{}", JSON.toJSONString(refuseResult));

        Result checkResult = activityExecStatusController.execStatus(activityId,Status.Editing,Status.Check);
        log.info("测试结果（编辑中->待审核）：{}", JSON.toJSONString(checkResult));

    }


}
