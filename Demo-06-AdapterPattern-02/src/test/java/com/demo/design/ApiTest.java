package com.demo.design;

import com.alibaba.fastjson.JSON;
import com.demo.design.cuisine.impl.InsideOrderService;
import com.demo.design.cuisine.impl.POPOrderAdapterService;
import com.demo.design.mq.CreatAccount;
import com.demo.design.mq.OrderMq;
import org.junit.Test;
import sun.misc.REException;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {

    @Test
    public void testMQAdapter() throws ParseException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = s.parse("2020-06-01 23:20:16");

        CreatAccount creatAccount = new CreatAccount();
        creatAccount.setNumber("100001");
        creatAccount.setAddress("河北省.廊坊市.广阳区.大学里职业技术学院");
        creatAccount.setAccountDate(parse);
        creatAccount.setDesc("在校开户");

        Map link01 = new HashMap<String, String>();
        link01.put("userId","number");
        link01.put("bizId","number");
        link01.put("bizTime","accountDate");
        link01.put("desc","desc");

        RebateInfo rebateInfo01 = MQAdapter.filter(creatAccount.toString(),link01);
        System.out.println("mq.creatAccount(适配前)"+creatAccount.toString());
        System.out.println("mq.creatAccount(适配后)"+ JSON.toJSONString(rebateInfo01));

        System.out.println("----------");

        OrderMq orderMq = new OrderMq();
        orderMq.setUid("100001");
        orderMq.setSku("10928092093111123");
        orderMq.setOrderId("100000890193847111");
        orderMq.setCreateOrderTime(parse);

        Map link02 = new HashMap<String, String>();
        link02.put("userId","uid");
        link02.put("bizId","orderId");
        link02.put("bizTime","createOrderTime");
        RebateInfo rebateInfo02 = MQAdapter.filter(orderMq.toString(),link02);
        System.out.println("mq.orderMq(适配前)" + orderMq.toString());
        System.out.println("mq.orderMq(适配后)" + JSON.toJSONString(rebateInfo02));
    }

    @Test
    public void testOrderAdapter() {
        OrderAdapterService popOrderAdapterService = new POPOrderAdapterService();
        System.out.println("判断首单，接口适配(POP)：" + popOrderAdapterService.isFirst("100001"));

        OrderAdapterService insideOrderService = new InsideOrderService();
        System.out.println("判断首单，接口适配(自营)：" + insideOrderService.isFirst("100001"));
    }

}
