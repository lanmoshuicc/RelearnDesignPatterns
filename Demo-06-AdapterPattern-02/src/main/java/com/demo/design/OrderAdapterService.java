package com.demo.design;

/**
 * @Author: chenchen19
 * @Description
 */
public interface OrderAdapterService {

    boolean isFirst(String uId);

}
