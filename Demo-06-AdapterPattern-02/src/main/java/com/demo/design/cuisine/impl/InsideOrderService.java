package com.demo.design.cuisine.impl;

import com.demo.design.OrderAdapterService;
import com.demo.design.service.OrderService;

/**
 * @Author: chenchen19
 * @Description
 */

public class InsideOrderService implements OrderAdapterService {

    private OrderService orderService = new OrderService();

    public boolean isFirst(String uId) {
        return orderService.queryUserOrderCount(uId) <= 1;
    }
}
