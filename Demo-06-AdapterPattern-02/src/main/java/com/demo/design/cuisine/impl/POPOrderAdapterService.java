package com.demo.design.cuisine.impl;

import com.demo.design.OrderAdapterService;
import com.demo.design.service.POPOrderService;

/**
 * @Author: chenchen19
 * @Description
 */

public class POPOrderAdapterService implements OrderAdapterService {

    private POPOrderService popOrderService = new POPOrderService();

    public boolean isFirst(String uId) {
        return popOrderService.isFirstOrder(uId);
    }
}
