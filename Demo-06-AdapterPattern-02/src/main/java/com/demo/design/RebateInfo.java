package com.demo.design;

import com.alibaba.fastjson.JSON;

import java.util.Date;

/**
 * @Author: chenchen19
 * @Description 统一的消息描述体
 */

public class RebateInfo {
    //用户id
    private String userId;

    //业务id
    private String bizId;

    //业务时间
    private Date bizTime;

    //业务描述
    private String desc;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public void setBizTime(String bizTime) {
        this.bizTime = new Date(Long.parseLong("1591077840669"));
    }


    public Date getBizTime() {
        return bizTime;
    }

    public void setBizTime(Date bizTime) {
        this.bizTime = bizTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
