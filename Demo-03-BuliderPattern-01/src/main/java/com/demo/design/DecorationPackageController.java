package com.demo.design;

import com.demo.design.ceiling.LevelOneCeiling;
import com.demo.design.ceiling.LevelTwoCeiling;
import com.demo.design.coat.DuluxCoat;
import com.demo.design.coat.LiBangCoat;
import com.demo.design.floor.ShengXiangFloor;
import com.demo.design.title.DongPengTile;
import com.demo.design.title.MarcoPoloTile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: chenchen19
 * @Description
 */

public class DecorationPackageController {

    public String getMatterList(BigDecimal area,Integer level) {

        List<Matter> matterList = new ArrayList<Matter>();
        BigDecimal price = BigDecimal.ZERO;

        // 豪华欧式
        if(level == 1) {
            //吊顶，二级顶
            LevelTwoCeiling levelTwoCeiling = new LevelTwoCeiling();

            //涂料，多乐士
            DuluxCoat duluxCoat = new DuluxCoat();

            //地板，圣象
            ShengXiangFloor shengXiangFloor = new ShengXiangFloor();

            matterList.add(levelTwoCeiling);
            matterList.add(duluxCoat);
            matterList.add(shengXiangFloor);

            price = price.add(area.multiply(new BigDecimal("0.2")).multiply(levelTwoCeiling.price()));
            price = price.add(area.multiply(new BigDecimal("1.4")).multiply(duluxCoat.price()));
            price = price.add(area.multiply(shengXiangFloor.price()));

        }

        //轻奢田园
        if(level == 2) {
            //吊顶，二级顶
            LevelTwoCeiling levelTwoCeiling = new LevelTwoCeiling();

            //涂料，立邦
            LiBangCoat liBangCoat = new LiBangCoat();

            //地砖，马可波罗
            MarcoPoloTile marcoPoloTile = new MarcoPoloTile();

            matterList.add(levelTwoCeiling);
            matterList.add(liBangCoat);
            matterList.add(marcoPoloTile);

            price = price.add(area.multiply(new BigDecimal("0.2")).multiply(levelTwoCeiling.price()));
            price = price.add(area.multiply(new BigDecimal("1.4")).multiply(liBangCoat.price()));
            price = price.add(area.multiply(marcoPoloTile.price()));

        }

        if(level == 3) {
            //吊顶，二级顶
            LevelTwoCeiling levelTwoCeiling = new LevelTwoCeiling();

            //涂料，立邦
            LiBangCoat liBangCoat = new LiBangCoat();

            //地砖，东鹏
            DongPengTile dongPengTile = new DongPengTile();

            matterList.add(levelTwoCeiling);
            matterList.add(liBangCoat);
            matterList.add(dongPengTile);

            price = price.add(area.multiply(new BigDecimal("0.2")).multiply(levelTwoCeiling.price()));
            price = price.add(area.multiply(new BigDecimal("1.4")).multiply(liBangCoat.price()));
            price = price.add(area.multiply(dongPengTile.price()));

        }

        StringBuilder detail = new StringBuilder("\r\n------------------------------------------------------\r\n"
                + "装修清单" + "\r\n"
                + "套餐等级：" + level + "\r\n"
                + "套餐价格：" + price.setScale(2, BigDecimal.ROUND_HALF_UP) + " 元\r\n"
                + "房屋⾯积：" + area.doubleValue() + " 平⽶\r\n"
                + "材料清单：\r\n");

        for(Matter matter:matterList) {
            detail.append(matter.scene()).append(":")
                    .append(matter.brand()).append(":")
                    .append(matter.model()).append("、平⽶价格：")
                    .append(matter.price()).append(" 元。\n");
        }

        return  detail.toString();
    }

}
