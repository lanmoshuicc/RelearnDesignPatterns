package com.demo.design;

import org.junit.Test;

import java.math.BigDecimal;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {

    @Test
    public void test() {
        DecorationPackageController decorationPackageController = new DecorationPackageController();
        String detail1 = decorationPackageController.getMatterList(new BigDecimal("20"), 1);
        System.out.println(detail1);

        String detail2 = decorationPackageController.getMatterList(new BigDecimal("20"),2);
        System.out.println(detail2);

        String detail3 = decorationPackageController.getMatterList(new BigDecimal("20"),3);
        System.out.println(detail3);
    }

}
