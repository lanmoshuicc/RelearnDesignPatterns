package com.demo.design;

import java.util.concurrent.TimeUnit;

/**
 * @Author: chenchen19
 * @Description
 */

public interface CacheService {

    String get(final String key);

    void set(String key,String val);

    void set(String key, String val, long timeout, TimeUnit timeUnit);

    void del(String key);

}
