package com.demo.design.cuisin.impl;

import com.demo.design.CacheService;
import com.demo.design.RedisUtils;

import java.util.concurrent.TimeUnit;

/**
 * @Author: chenchen19
 * @Description
 */

public class CacheServiceImpl implements CacheService {

    private RedisUtils redisUtils = new RedisUtils();

    public String get(String key) {
        return redisUtils.get(key);
    }

    public void set(String key, String val) {
        redisUtils.set(key,val);
    }

    public void set(String key, String val, long timeout, TimeUnit timeUnit) {
        redisUtils.set(key,val,timeout,timeUnit);
    }

    public void del(String key) {
        redisUtils.del(key);
    }
}
