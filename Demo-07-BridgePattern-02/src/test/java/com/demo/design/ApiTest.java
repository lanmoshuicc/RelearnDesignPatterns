package com.demo.design;

import com.demo.design.channel.AliPay;
import com.demo.design.channel.WeiXinPay;
import com.demo.design.model.PayFaceMode;
import com.demo.design.model.PayFingerPrintMode;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {

    @Test
    public void testPay() {
        System.out.println("\r\n模拟测试场景；微信⽀付、⼈脸⽅式。");
        WeiXinPay weiXinPay = new WeiXinPay(new PayFaceMode());
        weiXinPay.transfer("weixin_20200811001","20200811001001",new BigDecimal(100));

        System.out.println("\r\n模拟测试场景;支付宝支付、指纹方式。");
        AliPay aliPay = new AliPay(new PayFingerPrintMode());
        aliPay.transfer("alipay_20200811001","20200811001002",new BigDecimal(100));
    }

}
