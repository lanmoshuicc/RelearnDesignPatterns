package com.demo.design.channel;

import com.demo.design.model.IPayMode;

import java.math.BigDecimal;

/**
 * @Author: chenchen19
 * @Description
 */

public class AliPay extends Pay{
    public AliPay(IPayMode payMode) {
        super(payMode);
    }

    public String transfer(String uId, String tradeId, BigDecimal amount) {
        logger.info("模拟支付宝渠道⽀付划账开始。uId：{} tradeId：{} amount：{}", uId, tradeId, amount);
        boolean security = payMode.security(uId);
        logger.info("模拟⽀付宝渠道⽀付⻛风控校验。uId：{} tradeId：{} security： {}", uId, tradeId, security);
        if(!security) {
            logger.info("模拟支付宝渠道⽀付划账拦截。uId：{} tradeId：{} amount： {}", uId, tradeId, amount);
            return "0001";
        }
        logger.info("模拟支付宝渠道⽀付划账成功。uId：{} tradeId：{} amount： {}", uId, tradeId, amount);
        return "0000";
    }
}
