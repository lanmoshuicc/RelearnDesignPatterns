package com.demo.design.model;

/**
 * @Author: chenchen19
 * @Description
 */
public interface IPayMode {
    boolean security(String uId);
}
