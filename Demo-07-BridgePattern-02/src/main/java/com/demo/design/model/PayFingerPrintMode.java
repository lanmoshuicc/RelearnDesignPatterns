package com.demo.design.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: chenchen19
 * @Description
 */

public class PayFingerPrintMode implements IPayMode{

    protected Logger logger = LoggerFactory.getLogger(PayFingerPrintMode.class);

    public boolean security(String uId) {
        logger.info("指纹支付，风控校验指纹信息");
        return true;
    }
}
