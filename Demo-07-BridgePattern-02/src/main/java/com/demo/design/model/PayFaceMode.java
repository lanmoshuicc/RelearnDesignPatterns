package com.demo.design.model;

import javafx.scene.effect.Bloom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: chenchen19
 * @Description
 */

public class PayFaceMode implements IPayMode{

    protected Logger logger = LoggerFactory.getLogger(PayFaceMode.class);

    public boolean security(String uId) {
        logger.info("人脸支付，风控校验脸部识别");
        return true;
    }
}
