package com.demo.design;

/**
 * @Author: chenchen19
 * @Description
 */

public class SsoInterceptor implements HandlerInterceptor{
    public boolean preHandle(String request, String response, Object handler) {
        // 模拟获取cookie
        String ticket = request.substring(0,7);

        // 模拟校验
        return ticket.equals("success");
    }
}
