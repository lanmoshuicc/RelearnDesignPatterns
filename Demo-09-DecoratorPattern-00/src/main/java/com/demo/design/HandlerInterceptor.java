package com.demo.design;

/**
 * @Author: chenchen19
 * @Description
 */
public interface HandlerInterceptor {
    boolean preHandle(String request,String response,Object handler);
}
