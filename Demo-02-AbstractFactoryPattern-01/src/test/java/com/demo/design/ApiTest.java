package com.demo.design;

import com.demo.desgin.CacheService;
import com.demo.desgin.impl.CacheServiceImpl;
import org.junit.Test;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {

    @Test
    public void test_cacheService() {
        CacheService cacheService = new CacheServiceImpl();
        cacheService.set("user_name_01","小明",1);
        String val01 = cacheService.get("user_name_01",1);
        System.out.println(val01);
    }
}
