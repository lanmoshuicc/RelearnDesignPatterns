package com.demo.desgin.impl;

import com.demo.desgin.CacheService;
import com.demo.design.RedisUtils;
import com.demo.design.matter.EGM;
import com.demo.design.matter.IIR;

import java.util.concurrent.TimeUnit;

/**
 * @Author: chenchen19
 * @Description
 */

public class CacheServiceImpl implements CacheService {

    private EGM egm = new EGM();
    private IIR iir = new IIR();
    private RedisUtils redisUtils = new RedisUtils();

    public String get(String key,int redisType) {
        if (redisType == 1) {
            return egm.gain(key);
        } else if (redisType == 2) {
            return iir.get(key);
        }
        return redisUtils.get(key);
    }

    public void set(String key, String val,int redisType) {
        if (redisType == 1) {
            egm.set(key,val);
            return;
        } else if (redisType == 2) {
            iir.set(key,val);
            return;
        }
        redisUtils.set(key,val);
    }

    public void set(String key, String val, long timeout, TimeUnit timeUnit,int redisType) {
        if (1 == redisType) {
            egm.setEx(key, val, timeout, timeUnit);
            return;
        }

        if (2 == redisType) {
            iir.setExpire(key, val, timeout, timeUnit);
            return;
        }

        redisUtils.set(key, val, timeout, timeUnit);
    }

    public void del(String key,int redisType) {
        if (1 == redisType) {
            egm.delete(key);
            return;
        }

        if (2 == redisType) {
            iir.del(key);
            return;
        }

        redisUtils.del(key);
    }
}
