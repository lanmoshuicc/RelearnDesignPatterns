package com.demo.desgin;

import java.util.concurrent.TimeUnit;

/**
 * @Author: chenchen19
 * @Description
 */
public interface CacheService {
    String get(final String key,int redisType);

    void set(String key,String val,int redisType);

    void set(String key, String val, long timeout, TimeUnit timeUnit,int redisType);

    void del(String key,int redisType);
}
