package com.demo.design;

import com.demo.design.store.ICommodity;
import com.demo.design.store.impl.CouponCommodityService;
import com.demo.design.store.impl.GoodsCommodityService;
import com.demo.design.store.impl.IQiyiCardCommodityService;

/**
 * @Author: chenchen19
 * @Description
 */

public class StoreFactory {

    public ICommodity getCommodityService(Integer commodityType) {
        if (null == commodityType) {
            return null;
        }
        if (1 == commodityType) {
            return new CouponCommodityService();
        }
        if(2 == commodityType) {
            return new GoodsCommodityService();
        }
        if(3 == commodityType) {
            return new IQiyiCardCommodityService();
        }
        throw new RuntimeException("不存在的商品服务类型");
    }

}
