package com.demo.design.store;

import java.util.Map;

/**
 * @Author: chenchen19
 * @Description 接⼝的⼊参包括； ⽤户ID 、 奖品ID 、 业务ID 以及 扩展字段 ⽤于处理发放实物商品时的收获地址。
 */
public interface ICommodity {
    void sendCommodity(String uId, String commodityId, String bizId, Map<String,String> extMap) throws Exception;
}
