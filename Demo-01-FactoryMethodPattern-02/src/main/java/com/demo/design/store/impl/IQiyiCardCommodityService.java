package com.demo.design.store.impl;

import com.alibaba.fastjson.JSON;
import com.demo.design.store.ICommodity;
import com.demo.test.card.IQiYiCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @Author: chenchen19
 * @Description
 */

public class IQiyiCardCommodityService implements ICommodity {
    private Logger logger = LoggerFactory.getLogger(IQiyiCardCommodityService.class);
    @Override
    public void sendCommodity(String uId, String commodityId, String bizId, Map<String, String> extMap) throws Exception {
        IQiYiCardService iQiYiCardService = new IQiYiCardService();
        iQiYiCardService.grantToke(queryUserPhone(uId),commodityId);
        logger.info("请求参数[爱奇艺兑换卡] => uId：{} commodityId：{} bizId：{} extMap：{}", uId, commodityId, bizId, JSON.toJSON(extMap));
        logger.info("测试结果[爱奇艺兑换卡]：{}", "爱奇艺兑换卡发送成功");
    }

    private String queryUserPhone(String uId) {
        return "15200101232";
    }
}
