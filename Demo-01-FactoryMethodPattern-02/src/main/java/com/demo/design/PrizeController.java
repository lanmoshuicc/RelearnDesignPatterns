package com.demo.design;

import com.demo.design.store.ICommodity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: chenchen19
 * @Description
 */

public class PrizeController {
    private Logger logger = LoggerFactory.getLogger(PrizeController.class);

    public AwardRes awardToUser(AwardReq req) {
        StoreFactory storeFactory = new StoreFactory();
        AwardRes res = null;
        ICommodity commdity = storeFactory.getCommodityService(req.getAwardType());
        try {
            commdity.sendCommodity(req.getuId(),req.getAwardNumber(),req.getBizId(),req.getExtMap());
            res = new AwardRes("0000","发送成功！");
        } catch (Exception e) {
            e.printStackTrace();
            res = new AwardRes("0001","发送失败！");
        }

        return res;
    }

}