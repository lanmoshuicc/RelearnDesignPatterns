package com.demo.design;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: chenchen19
 * @Description
 */

public class LoginSsoDecorator extends SsoInterceptor {
    private static Map<String,String> authMap = new ConcurrentHashMap<>();

    static {
        authMap.put("huahua","queryUserInfo");
        authMap.put("doudou","queryUserInfo");
    }

    @Override
    public boolean preHandle(String request, String response, Object handler) {
        String ticket = request.substring(0,7);
        boolean success = ticket.equals("success");
        if(!success) {
            return false;
        }
        String userId = request.substring(7);
        String method = authMap.get(userId);

        return "queryUserInfo".equals(method);
    }
}
