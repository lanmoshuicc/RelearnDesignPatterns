package com.demo.design;

import org.junit.Test;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {

    @Test
    public void testLoginSsoDecorator() {
        LoginSsoDecorator loginSsoDecorator = new LoginSsoDecorator();
        String request = "successhuahua";
        boolean success = loginSsoDecorator.preHandle(request,"jafjaj","test");
        System.out.println("登录校验："+request+(success?"放行":"拦截"));
    }
}
