package com.demo.design;

import org.junit.Test;

import java.math.BigDecimal;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {

    @Test
    public void testPay() {
        PayController payController = new PayController();
        System.out.println("\r\n模拟测试场景：微信支付、人脸方式");
        payController.doPay("weixin_20200811001","20200811001001",new BigDecimal("100"),1,2);

        System.out.println("\r\n模拟测试场景：支付宝支付、指纹方式");
        payController.doPay("alipay_20200811001","20200811001002",new BigDecimal("100"),2,3);
    }

}
