package com.demo.design;

import com.demo.design.ceiling.LevelOneCeiling;
import com.demo.design.ceiling.LevelTwoCeiling;
import com.demo.design.coat.DuluxCoat;
import com.demo.design.coat.LiBangCoat;
import com.demo.design.floor.ShengXiangFloor;
import com.demo.design.title.DongPengTile;
import com.demo.design.title.MarcoPoloTile;

import java.math.BigDecimal;

/**
 * @Author: chenchen19
 * @Description
 */

public class Bulider {

    public IMenu levelOne(Double area) {
        return new DecorationPackageMenu(new BigDecimal(area),"欧式豪华")
                .appendCeiling(new LevelTwoCeiling())
                .appendCoat(new DuluxCoat())
                .appendFloor(new ShengXiangFloor());
    }

    public IMenu levelTwo(Double area) {
        return new DecorationPackageMenu(new BigDecimal(area),"轻奢田园")
                .appendCeiling(new LevelTwoCeiling())
                .appendCoat(new LiBangCoat())
                .appendTile(new MarcoPoloTile());
    }

    public IMenu levelThree(Double area) {
        return new DecorationPackageMenu(new BigDecimal(area),"现代简约")
                .appendCeiling(new LevelTwoCeiling())
                .appendCoat(new LiBangCoat())
                .appendTile(new DongPengTile());
    }

}
