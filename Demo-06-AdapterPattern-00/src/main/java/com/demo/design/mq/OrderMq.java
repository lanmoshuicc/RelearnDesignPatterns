package com.demo.design.mq;

import com.alibaba.fastjson.JSON;

import java.util.Date;

/**
 * @Author: chenchen19
 * @Description 订单消息
 */

public class OrderMq {

    //用户Id
    private String uid;

    //商品
    private String sku;

    //订单id
    private String orderId;

    //下单时间
    private Date createOrderTime;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getCreateOrderTime() {
        return createOrderTime;
    }

    public void setCreateOrderTime(Date createOrderTime) {
        this.createOrderTime = createOrderTime;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
