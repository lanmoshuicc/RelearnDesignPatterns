package com.demo.design;

import com.alibaba.fastjson.JSON;
import com.demo.design.mq.CreatAccount;

public class CreatAccountMqService {

    public void onMessage(String message) {

        CreatAccount mq = JSON.parseObject(message, CreatAccount.class);

        mq.getNumber();
        mq.getAccountDate();

        // ... 处理自己的业务
    }

}
