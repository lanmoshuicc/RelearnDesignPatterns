package com.demo.design;

import org.junit.Test;

/**
 * @Author: chenchen19
 * @Description
 */

public class ApiTest {

    @Test
    public void test_creatPager() throws CloneNotSupportedException {
        QuestionBankController questionBankController = new QuestionBankController();
        System.out.println(questionBankController.createPager("小明", "1"));
        System.out.println(questionBankController.createPager("小红", "2"));
        System.out.println(questionBankController.createPager("小毛", "3"));
    }

}
