package com.demo.design.util;

import java.util.*;

/**
 * @Author: chenchen19
 * @Description
 */

public class TopicRandomUtil {
    /**
     * 乱序Map元素，记录对应答案key
     * @param option
     * @param key
     * @return
     */

    public static Topic random(Map<String,String> option, String key) {

        Set<String> keyset = option.keySet();
        ArrayList<String> keyList = new ArrayList<String>(keyset);
        Collections.shuffle(keyList);
        HashMap<String, String> newOption = new HashMap<String,String>();
        int idx = 0;
        String newKey = "";
        for(String next:keyset) {
            String randomKey = keyList.get(idx++);
            if (key.equals(next)) {
                newKey = randomKey;
            }
            newOption.put(randomKey,option.get(next));
        }
        return new Topic(newOption,newKey);
    }

}
