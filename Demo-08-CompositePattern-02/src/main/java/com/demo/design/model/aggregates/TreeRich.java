package com.demo.design.model.aggregates;

import com.demo.design.model.vo.TreeNode;
import com.demo.design.model.vo.TreeRoot;

import java.util.Map;

/**
 * @Author: chenchen19
 * @Description 规则树集合
 */

public class TreeRich {
    // 树根信息
    private TreeRoot treeRoot;
    // 树节点Id -> 子节点
    private Map<Long, TreeNode> treeNodeMap;

    public TreeRich(TreeRoot treeRoot, Map<Long, TreeNode> treeNodeMap) {
        this.treeRoot = treeRoot;
        this.treeNodeMap = treeNodeMap;
    }

    public TreeRoot getTreeRoot() {
        return treeRoot;
    }

    public void setTreeRoot(TreeRoot treeRoot) {
        this.treeRoot = treeRoot;
    }

    public Map<Long, TreeNode> getTreeNodeMap() {
        return treeNodeMap;
    }

    public void setTreeNodeMap(Map<Long, TreeNode> treeNodeMap) {
        this.treeNodeMap = treeNodeMap;
    }
}
