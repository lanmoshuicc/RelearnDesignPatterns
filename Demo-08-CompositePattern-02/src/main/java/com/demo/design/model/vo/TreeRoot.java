package com.demo.design.model.vo;

/**
 * @Author: chenchen19
 * @Description 树根信息
 */

public class TreeRoot {
    // 规则树Id
    private Long treeId;

    // 规则树根Id
    private Long treeRootNodeId;

    // 规则书名称
    private String treeName;

    public Long getTreeId() {
        return treeId;
    }

    public void setTreeId(Long treeId) {
        this.treeId = treeId;
    }

    public Long getTreeRootNodeId() {
        return treeRootNodeId;
    }

    public void setTreeRootNodeId(Long treeRootNodeId) {
        this.treeRootNodeId = treeRootNodeId;
    }

    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }
}
