package com.demo.design.service.logic;

import com.demo.design.model.vo.TreeNodeLink;

import java.util.List;
import java.util.Map;

/**
 * @Author: chenchen19
 * @Description
 */

public abstract class BaseLogic implements LogicFilter {
    public Long filter(String matterValue, List<TreeNodeLink> treeNodeLineInfoList) {
        for(TreeNodeLink nodeLink:treeNodeLineInfoList) {
            if(decisionLogic(matterValue,nodeLink)) {
                return nodeLink.getNodeIdTo();
            }
        }
        return 0L;
    }

    @Override
    public abstract String matterValue(Long treeId, String userId, Map<String, String> decisionMatter);

    private boolean decisionLogic(String matterValue,TreeNodeLink nodeLink) {
        switch (nodeLink.getRuleLimitType()) {
            case 1:
                return matterValue.equals(nodeLink.getRuleLimitValue());
            case 2:
                return Double.parseDouble(matterValue) > Double.parseDouble(nodeLink.getRuleLimitValue());
            case 3:
                return Double.parseDouble(matterValue) < Double.parseDouble(nodeLink.getRuleLimitValue());
            case 4:
                return Double.parseDouble(matterValue) <= Double.parseDouble(nodeLink.getRuleLimitValue());
            case 5:
                return Double.parseDouble(matterValue) >= Double.parseDouble(nodeLink.getRuleLimitValue());
            default:
                return false;
        }
    }
}
