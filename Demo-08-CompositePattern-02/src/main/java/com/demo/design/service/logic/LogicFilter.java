package com.demo.design.service.logic;

import com.demo.design.model.vo.TreeNodeLink;

import java.util.List;
import java.util.Map;

/**
 * @Author: chenchen19
 * @Description
 */
public interface LogicFilter {

    /**
     * 逻辑决策器
     * @param matterValue 决策值
     * @param treeNodeLineInfoList 决策节点
     * @return
     */
    Long filter(String matterValue, List<TreeNodeLink> treeNodeLineInfoList);

    /**
     * 获取决策值
     * @param treeId
     * @param userId
     * @param decisionMatter 决策物料
     * @return
     */
    String matterValue(Long treeId, String userId, Map<String,String> decisionMatter);

}
