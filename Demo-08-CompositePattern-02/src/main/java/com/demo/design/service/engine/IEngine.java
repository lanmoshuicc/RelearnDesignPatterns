package com.demo.design.service.engine;

import com.demo.design.model.aggregates.TreeRich;
import com.demo.design.model.vo.EngineResult;

import java.util.Map;

/**
 * @Author: chenchen19
 * @Description
 */
public interface IEngine {
    EngineResult process(final Long treeId, final String userId, TreeRich treeRich,final Map<String,String> decisionMatter);
}
