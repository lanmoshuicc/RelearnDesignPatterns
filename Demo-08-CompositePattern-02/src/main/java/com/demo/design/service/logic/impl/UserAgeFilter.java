package com.demo.design.service.logic.impl;

import com.demo.design.service.logic.BaseLogic;

import java.util.Map;

/**
 * @Author: chenchen19
 * @Description
 */

public class UserAgeFilter extends BaseLogic {
    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("age");
    }
}
