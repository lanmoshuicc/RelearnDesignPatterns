package com.demo.design;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: chenchen19
 * @Description
 */

public class LoginSsoDecorator extends SsoDecorator{

    private Logger logger = LoggerFactory.getLogger(LoginSsoDecorator.class);

    private static Map<String,String> authMap = new ConcurrentHashMap<String,String>();;

    static {
        authMap.put("huahua","queryUserInfo");
        authMap.put("doudou","queryUserInfo");
    }

    public LoginSsoDecorator(SsoInterceptor ssoInterceptor) {
        super(ssoInterceptor);
    }

    @Override
    public boolean preHandle(String request, String response, Object handler) {
        boolean success = super.preHandle(request, response, handler);
        if (!success) return false;
        String userId = request.substring(7);
        String method = authMap.get(userId);
        logger.info("模拟单点登录⽅法访问拦截校验：{} {}", userId, method); // 模拟⽅法校验
        return "queryUserInfo".equals(method);
    }

}
