package com.demo.test.card;

/**
 * @Author: chenchen19
 * @Description 模拟爱奇艺会员卡服务
 */

public class IQiYiCardService {
    public void grantToke(String bindMobileNumber,String cardId) {
        System.out.println("模拟发放爱奇艺会员卡一张："+bindMobileNumber + "，" + cardId);
    }
}
