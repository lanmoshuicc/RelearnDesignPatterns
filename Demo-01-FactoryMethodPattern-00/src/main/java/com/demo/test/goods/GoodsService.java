package com.demo.test.goods;

import com.alibaba.fastjson.JSON;

/**
 * @Author: chenchen19
 * @Description 模拟实物商品服务
 */

public class GoodsService {
    public Boolean deliverGoods(DeliverReq req) {
        System.out.println("模拟发货实物商品一个：" + JSON.toJSONString(req));
        return true;
    }
}
